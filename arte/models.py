from django.db import models
from datetime import date

# Create your models here.
class TipoArte(models.Model):
    nombre = models.CharField(max_length=100)


class Artista(models.Model):
    nombre = models.CharField(max_length=100)
    nombre_artistico = models.CharField(max_length=100)
    edad = models.IntegerField()
    tipo_arte = models.ForeignKey(TipoArte,on_delete=models.CASCADE, related_name='tipoarte')

