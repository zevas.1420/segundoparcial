from arte.models import TipoArte, Artista

def cargar_arte_base(nombre):
    lista_arte = TipoArte.objects.all()
    for item in lista_arte:
        if item.nombre == nombre:
            raise Exception("Error: el nombre de arte ya existe")
        if nombre == '':
            raise Exception("Error: el nombre no puede estar vacio")

    tipoarte = TipoArte()
    tipoarte.nombre = nombre
    tipoarte.save()


def cargar_artista_base(nombre,nombre_artistico,edad,tipo_arte_id):
    lista_artista = Artista.objects.all()
    lista_arte = TipoArte.objects.all()

    for item in lista_artista:
        if item.nombre_artistico == nombre_artistico: #valido que no exista el nombre en el listado
            raise Exception("Error: el nombre artistico ya existe")  #errores creados 
        if nombre_artistico == '':
            raise Exception("Error: el nombre artistico no puede estar vacio")
        if nombre == '':
            raise Exception("Error: el nombre no puede estar vacio")
        if int(edad) < int(18): #valido que sea mayor de 18
            raise Exception("Error: la edad debe ser mayor a 18 años")


    artista = Artista() #creo el artista
    artista.nombre = nombre
    artista.nombre_artistico = nombre_artistico
    artista.edad = edad
    artista.tipo_arte_id = tipo_arte_id
    artista.save() # lo guardo

