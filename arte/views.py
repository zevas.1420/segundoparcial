from django.shortcuts import render, redirect
from arte.models import TipoArte, Artista
from arte.helpers.cargas import cargar_arte_base, cargar_artista_base
from django.db import IntegrityError

# Create your views here.

def cargar_tipoarte(request):
    lista_arte = TipoArte.objects.all().order_by('nombre')

    if request.method == 'POST': #verifico que sea metodo POST lo enviado
        try: #inicio la toma de excepciones
            nombre = request.POST.get('nombre') #obtengo nombre del form HTML y lo agrego a la variable nombre
            
            cargar_arte_base(nombre) #realizo un verificador en helpers.cargas donde voy a verificar el nombre que no se repita
            
        except IntegrityError as dbErr: #verifico errores de base de datos
            return render(request,'cargar_tipoarte.html',{'lista_arte':lista_arte, 'error': str(dbErr)}) #retorno lista y error la pagina
        except Exception as err:
            return render(request, 'cargar_tipoarte.html', {'lista_arte': lista_arte, 'error': str(err)})

    return render(request, 'cargar_tipoarte.html', {'lista_arte': lista_arte}) #retorno datos la pagina html

def cargar_artista(request):
    lista_artista = Artista.objects.all()
    lista_arte = TipoArte.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            nombre_artistico = request.POST.get('nombre_artistico')
            edad = request.POST.get('edad')
            tipo_arte_id = request.POST.get('tipo_arte_id')
            
            cargar_artista_base(nombre,nombre_artistico,edad,tipo_arte_id)
            
        except IntegrityError as dbErr:
            return render(request,'cargar_artista.html',{'lista_artista':lista_artista,'error':str(dbErr)})
        except Exception as err:
            return render(request, 'cargar_artista.html', {'lista_artista': lista_artista,'lista_arte':lista_arte, 'error': str(err)})
        
    return render(request, 'cargar_artista.html', {'lista_artista': lista_artista, 'lista_arte':lista_arte})

def eliminar_artista(request,id_artista):

    artista = Artista.objects.get(id=id_artista).delete()  #el id que obtengo lo uso para eliminar

    return redirect('/arte/cargarartista') #redirecciono la pagina a cargas

def inicio(request):
    return render(request, 'index.html')


